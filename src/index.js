 
const input = require('readline');
const rl = input.createInterface({
    input: process.stdin,
    output: process.stdout
});

/**
 * 
 * @param {*} input 
 * @returns {}user input value from console
 */
const getUserData = (input) =>{
    // get input from user
    return new Promise((resolve, reject) => {
        let result;
        rl.question(input, (uinput) => {
            result = parseInt(uinput);
            // if (result === 0 || result <= 0) {
            //     throw new Error('input is not valid');
            // }
            resolve(result);
        }, (error) => {
            reject(error);
        });
    });
}

/**
 * main function
 */
async function snakeLadderGame() {

    let position, diceValue;
    let presentValue = 0;
    const n = 10;
    //  ladder position
    const ladderMap = {
        7: 33,
        37: 85,
        51: 72
    }
    // snake position:
    const snakeMap = {
        36: 19,
        65: 35,
        87: 32
    }
    let finalResult ;
    for (let i = presentValue; presentValue <= 105; i++) {
        position = await getUserData("Enter current position:");
        position = (position <= 0)? await getUserData("Enter current position:"):position;
        diceValue = await getUserData("Enter current diceValue:");
        diceValue = (diceValue>6) ? await getUserData("Enter valid diceValue:"): diceValue;
        presentValue = diceValue + position;
        finalResult = await changeCurrentPosition(presentValue, diceValue, ladderMap, snakeMap, n);
    };
    console.log("presentValue in snake",finalResult);
    return finalResult;
};

/**
 * 
 * @param {*} presentValue 
 * @param {*} diceValue 
 * @param {*} ladderMap 
 * @param {*} snakeMap 
 * @param {*} n 
 */
const changeCurrentPosition = (presentValue, diceValue, ladderMap, snakeMap, n)=> {
    console.log("presentValue",presentValue);
    if (presentValue === (n * n)) {
        console.log("Yay!! You won!! ");
        process.exit();
    }
    if (snakeMap[presentValue]) {
        console.log(`Oppsss! You're at ${presentValue} & a snake has bitten you go back to ${snakeMap[presentValue]}`)
        presentValue = snakeMap[presentValue];
    }
    if (ladderMap[presentValue]) {
        console.log(`Yayyy! You're at ${presentValue},you got a ladder go to ${ladderMap[presentValue]}`)
        presentValue = ladderMap[presentValue];
    }
    if (presentValue > (n * n)) {
        presentValue = presentValue - diceValue;
    }
    console.log("New Position value=====>", presentValue);
    return presentValue;
}

snakeLadderGame();

module.exports = {
    getUserData,
    snakeLadderGame,
    changeCurrentPosition
}