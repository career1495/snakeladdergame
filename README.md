## Name

# snakeLadderGame

## Description
This is a simple console based Application of Snakes & Ladders game. 

## Prerequisites

This project requires NodeJS (version 8 or later) and NPM.
[Node](http://nodejs.org/) and [NPM](https://npmjs.org/) are really easy to install.
To make sure you have them available on your machine,
try running the following command.

```sh
$ npm -v && node -v
6.4.1
v8.16.0
```

## Table of contents

- [Project Name](#project-name)
  - [Prerequisites](#prerequisites)
  - [Table of contents](#table-of-contents)
  - [Getting Started](#getting-started)
  - [Installation](#installation)
  - [Usage](#usage)
    - [Serving the app](#serving-the-app)
    - [Sample output of app](#sample-output-of-app)
    - [Running the tests](#running-the-tests)
  - [Authors](#authors)
  - [License](#license)

## Getting started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.


## Installation

**BEFORE YOU INSTALL:** please read the [prerequisites](#prerequisites)

Start with cloning this repo on your local machine:

```
git clone https://gitlab.com/career1495/snakeladdergame.git
cd snakeladdergame
```

To install and set up the library, run:

```sh
$ npm install readline jest
```

***


## Usage

### Serving the app

```sh
$ npm start
```
### Sample output of the app

- Enter current position: 4
- Enter current diceValue: 5
- presentValue 9
- New Position value=====> 9
- Enter current position:34
- Enter current diceValue:3
- presentValue 37
- Yayyy! You're at 37,you got a ladder go to 85
- New Position value=====> 85
- Enter current position:83
- Enter current diceValue:4
- presentValue 87
- Oppsss! You're at 87 & a snake has bitten you go back to 32
- New Position value=====> 32
- Enter current position:96
- Enter current diceValue:5
- presentValue 101
- New Position value=====> 96
- Enter current position:99
- Enter current diceValue:1
- presentValue 100
- Yay!! You won!!


### Running the tests

```sh
$ npm test
```

## Authors and acknowledgment
- Ganesh P Apune


