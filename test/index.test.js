const { changeCurrentPosition } = require('../src/index');
const {ladderMap,snakeMap,input} = require("../test/helper");

test('it should be new position', async () => {
    let presentValue = input.diceValue + input.position;
    expect(changeCurrentPosition(presentValue,input.diceValue,ladderMap,snakeMap,input.n)).toStrictEqual(presentValue)
})

test('it should be snake bitten case', async () => {
    let presentValue = input.snakePosition + input.snakeDiceValue;
    expect(changeCurrentPosition(presentValue,input.snakeDiceValue,ladderMap,snakeMap,input.n)).toStrictEqual(snakeMap[presentValue]);
})

test('it should be ladder case', async () => {
    let presentValue = input.ladderDiceValue + input.ladderPosition;
    expect(changeCurrentPosition(presentValue,input.ladderDiceValue,ladderMap,snakeMap,input.n)).toStrictEqual(ladderMap[presentValue])
})

test('it should be more than 100', async () => {
    let presentValue = input.morePosition + input.moreDiceValue;
    expect(changeCurrentPosition(presentValue,input.moreDiceValue,ladderMap,snakeMap,input.n)).toStrictEqual(presentValue - input.moreDiceValue);
})