module.exports.ladderMap = {
    7: 33,
    37: 85,
    51: 72
}
// snake position:
module.exports.snakeMap = {
    36: 19,
    65: 35,
    87: 32
}


module.exports.input = {
diceValue : 5,
n : 10,
position : 4,
snakePosition : 83,
snakeDiceValue : 4,
ladderPosition : 34,
ladderDiceValue : 3,
morePosition : 99,
moreDiceValue:5
}
